---
title: Interviewpartner*innen gesucht
draft: false
menu:
  main:
    name: "Interviewpartner*innen gesucht"
    url: "/interviews"
    weight: 5
---

Um das Erleben und die unterschiedlichen Bewältigungsweisen des Alltags in Zeiten der Corona-Pandemie zu untersuchen, sind wir auf Ihre Hilfsbereitschaft angewiesen.

Wir suchen Gesprächspartner_innen die sich vorstellen können in einem offenen Interview über ihren Alltag zur Zeit und über Herausforderungen und Sorgen ebenso wie über positive Erlebnisse oder ihre Einschätzungen zu berichten.
  - Sind Sie über 18 Jahre alt und leben in Hamburg?
  - Ja? Dann freuen uns auf ein Gespräch!

## Vorgehen der Erhebung

Sie müssen sich hierfür in keiner Weise vorbereiten, alles was Sie benötigen ist ein wenig Zeit.  Zeitlich rechnen wir mit einem Aufwand von etwa 1 Stunde. Falls Sie gern teilnehmen möchten, aber nur wenig Zeit haben, kontaktieren Sie und bitte trotzdem, damit wir gemeinsam nach einem Weg suchen können.

Solange dies notwendig ist werden die Gespräche ohne einen persönlichen Kontakt geführt. Je nachdem was Ihnen lieber ist nutzen wir hierfür das Telefon oder Online-Dienste.

Selbstverständlich gehen wir vertraulich mit Ihren Daten um und werden die Interviews anonymisieren. (Weitere Informationen hierzu finden Sie im Abschnitt zum Datenschutz auf diesem Blog.)

Leider hat die Forschungswerkstatt nur begrenzte Sprach- und Übersetzungsressourcen, so dass wir lediglich auf deutsch, englisch und spanisch Interviews führen können.

Falls Sie sich eine Teilnahme vorstellen können senden Sie uns bitte eine kurze Mail mit *Ich bin dabei* an die folgende Kontaktadresse:

{{< mail_address class="mail-address" address="few.corona.eh@rauheshaus.de">}}

Wir rufen Sie dann im Laufe der nächsten Tage zurück oder melden uns per Mail bei Ihnen.

Vielen Dank für Ihre Hilfsbereitschaft - wir freuen uns bald von Ihnen zu hören!
