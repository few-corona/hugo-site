---
title: "Kooperationspartner Poliklinik"
draft: false
menu:
  main:
    name: "Kooperationspartner Poliklinik"
    url: "/poliklinik"
    weight: 20
---
{{< row class="row" >}}
  {{< column class="column" >}}
    {{< column_content >}}
Weitere Informationen zum Kooperationsprojekt *Poliklinik Veddel* finden Sie hier:  
    {{< /column_content >}}
    {{< link class="" src="https://poliklinik-veddel.org/" text="poliklinik1.org" >}}
  {{< /column >}}
  {{< column class="column column-center" >}}
    {{< column_content >}}
![Logo der Poliklinik](POLI_3_800x800_weiss.png)
    {{< /column_content >}}
  {{< /column >}}
{{< /row >}}
