---
draft: false
menu:
  main:
    name: "Welcome"
    url: "/"
    weight: 1
---
Welcome to our page,  
we are happy that you found us!

We are a research and development group from the Protestant University for Social Work called *Rauhes Haus*. In cooperation with the *Poliklinik Veddel*, we are conducting a research about social injustice and coping with everyday life during the Sars-Cov-2 pandemic.

The different hurdles that people may face during this time are what lead to this research study. While some people are at high risk to contract the virus and barely leave their house, others face the difficulties of watching their children while working a full-time job. Some demand the restrictions to be lifted, others demand to continue the *lock down*. While there are people living in affordable, small apartments in the city centre, others are very lonely during these desperate times and do not know how to pay their bills.

In a nutshell, everyone is experiencing certain challenges and is coping with them differently.

To understand these challenges and problems of everyday life, we need your help. We are seeking individuals that are willing to share their experience in an open interview.

Are you at least 18 years old? Do you live in Hamburg?  
Yes? Then we would be excited to interview you!  
You do not have to prepare yourself at all, all you need is a little time to spare.  

If necessary, we will conduct the interview without personal contact. Depending on what you prefer we will either have an interview on the phone or via online applications.

We estimate that the time necessary for the interview will be about one hour. If you do not have that much time, you may still contact us and we will adjust the conversation.

Your data will be handled with confidentiality and all interviews will be anonymized.

If you are interested in participating in our research, simply send us a message saying *Count me in* to the following e-mail address:  

{{< mail_address class="mail-address" address="few.corona.eh@rauheshaus.de">}}

We will either call you back or reply to your e-mail within a few days.

Thank you for your cooperation – we are excited to hear from you soon and we wish you all the best during these hard times!
