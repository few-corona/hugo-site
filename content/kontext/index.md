---
title: "Kontext der Forschung"
draft: false
menu:
  main:
    name: "Forschungkontext"
    url: "/kontext"
    weight: 15
---
Die Forschung findet im Rahmen einer Forschungs- und Entwicklungswerkstatt an der Ev. Hochschule für Soziale Arbeit und Diakonie *Rauhes Haus* unter der Leitung der Dipl. Politikwissenschaftlerin Anna Köster-Eiserfunke statt. Das Lehr-Forschungsprojekt hat bereits im Oktober 2019 begonnen und Studierende des Masterstudiengangs *Soziale Arbeit* erlernen hier durch praktische Forschungsarbeit und deren theoriegeleitete Reflexion und Vertiefung die Entwicklung, Planung und Durchführung empirischer Praxisforschung.

Als Kooperationsprojekt mit dem Stadteilgesundheitszentrum *Poliklinik Veddel* beschäftigt sich die Studierendengruppe sowohl mit Ansätzen qualitativer Sozialforschung als auch mit sozialen Determinanten von Gesundheit, deren Relevanz im Kontext der Sars-CoV-2-Pandemie verstärkt zu Tage tritt. Soziale Determinaten von Gesundheit sind gesellschaftliche Einflussfaktoren, die sich auf Gesundheit und Krankheit auswirken können und umfassen beispielsweise Wohn- und Arbeitsbedingungen, Infrastrukturen oder Diskriminierungserfahrungen.

Das Sars-CoV-2-Virus führt momentan auf der ganzen Welt zu Ausnahmezuständen. Aufgrund der Aktualität, der umfänglichen Auswirkungen und gesellschaftlichen Relevanz konzentriert sich die Forschungsarbeit seit März 2020 vollständig auf das (unterschiedliche) Erleben und Bewältigen der schwierigen Situation in verschiedenen Stadtteilen Hamburgs.

Zum Abschluss der Forschungs- und Entwicklungswerkstatt im Frühjahr 2021 werden die Ergebnisse dem Stadtteilgesundheitszentrum Poliklinik zur Reflexion und Fortentwicklung ihrer Praxis zugänglich gemacht.

Darüber hinaus sollen die Ergebnisse im Rahmen öffentlicher Workshops und/oder wissenschaftlicher Publikationen und Vorträge auch der interessierten Öffentlichkeit vorgestellt werden.
Weitere Informationen zu den Forschungs- und Entwicklungswerkstätten der Ev. Hochschule für Soziale Arbeit und Diakonie *Rauhes Haus* finden Sie hier:  

{{< link class="" src="https://www.ev-hochschule-hh.de/forschung-und-fortbildung/forschungswerkstaetten/" text="Forschungswerkstätten an der EHH" >}}
