---
title: Informationen zum Datenschutz
draft: false
menu:
  main:
    name: "Informationen zum Datenschutz"
    url: "/datenschutz"
    weight: 20
---
Falls Sie sich eine Teilnahme an der Forschung zur Alltagsbewältigung in Zeiten der Corona-Pandemie vorstellen können, finden Sie im Folgenden alle wesentlichen Informationen zum Umgang mit den im Kontext des Forschungsvorhabens geführten Interviews. Sollten darüber hinaus noch Fragen zur Umsetzung entstehen, stehen wir Ihnen mit Ihren Fragen selbstverständlich gern zur Verfügung.

Der Datenschutz verlangt Ihre ausdrückliche und informierte Einwilligung, was wir mit Ihrem Interview machen dürfen.

Die verantwortliche Leitung des Lehr-Forschungsprojektes liegt bei Dipl. Pol. Anna Köster-Eiserfunke und die Durchführung des Lehr- Forschungsprojektes entspricht der Datenschutzgrundverordnung und hält den Ethik-Kodex der Deutschen Gesellschaft für Soziologie ein.

Die an dem Lehr-Forschungsprojekt Beteiligten unterliegen der Schweigepflicht und sind auf das Datengeheimnis verpflichtet. Das Lehr-Forschungsprojekt dient allein wissenschaftlichen Zwecken. Wir sichern Ihnen folgendes Verfahren zu, damit Ihre Angaben nicht mit Ihrer Person in Verbindung gebracht werden können:

- Wir gehen sorgfältig mit dem Erzählten um: Wir nehmen das Gespräch, bei Einwilligung, auf. Die Aufnahme wird abgetippt und nach Abschluss der Forschungs- und Entwicklungswerkstatt, spätestens im April 2021, gelöscht. Die Abschrift (Transkript) können Sie erhalten.

- Wir anonymisieren das Transkript, d.h. wir verändern die Namen aller genannten Personen, so dass Sie von Außenstehenden nicht identifiziert werden können. Falls es notwendig erscheint werden wir durch die Veränderung von weiteren personenbezogenen Angaben ihre Anonymität zu gut als möglich zu schützen suchen.

- Ihr Name und Ihre Kontaktdaten werden am Ende des Projektes in unseren Unterlagen gelöscht, so dass lediglich das anonymisierte Transkript existiert. Die von Ihnen schriftlich erteilte oder aufgrund der Social Distancing-Maßnahmen lediglich mündlich formulierte und von uns digital aufgezeichnete Erklärung zur Einwilligung in die Auswertung wird auf einem gesonderten Speichermedium bzw. in einem gesonderten Ordner aufbewahrt. Sie dient lediglich dazu, bei einer Überprüfung durch den oder die Datenschutzbeauftragte_n nachweisen zu können, dass Sie mit der Auswertung einverstanden sind. Sie kann mit Ihrem transkribierten Interview nicht mehr in Verbindung gebracht werden.

- Wenn Ihre Zustimmung erfolgt ist, wird das Transkript für das Lehr-Forschungsprojekt verwendet und innerhalb des Seminars interpretiert sowie in in Form von Zitaten für den Forschungsbericht und Veröffentlichungen der Ergebnisse verwendet. Selbstverständlich sind alle Beteiligten im Rahmen der Forschungswerkstatt dazu verpflichtet diskret und vertraulich mit Ihren Daten umzugehen sowie die Anonymität der ForschungspartnerInnen zu wahren.

Die Datenschutzbestimmungen verlangen auch, dass wir Sie noch einmal ausdrücklich darauf hinweisen, dass aus einer Nichtteilnahme keine Nachteile entstehen. Sie können Antworten auch bei einzelnen Fragen verweigern. Auch die Einwilligung ist freiwillig und kann jederzeit von Ihnen widerrufen und die Löschung des Interviews von Ihnen verlangt werden. Wie bereits erwähnt, verlangt der Datenschutz Ihre ausdrückliche und informierte Einwilligung, was wir mit Ihrem Interview machen dürfen.

Wir erbitten im Falle einer Teilnahme an der Forschung um Ihre Zustimmung dafür,

- dass das Interview digital aufgezeichnet werden darf
- dass das Interview transkribiert und anonymisiert werden darf
- dass das Interview in transkribierter und anonymisierter Form im Rahmen des oben angegebenen Lehr-Forschungsprojektes im Seminar vor Ort interpretiert werden dürfen
- dass Auszüge und Zitate des transkribierten und anonymisierten Interviews in Veröffentlichungen zum Forschungsprojekt genutzt werden dürfen
- dass das transkribierte und anonymisierte Interview durch die Leitung der Forschungs- und Entwicklungswerkstatt, Frau Anna Köster-Eiserfunke (Dipl. Pol.), zu wissenschaftlichen Forschungszwecken weiterverwendet werden darf

### Anmerkung:
In der Formulierung dieser Hinweise zum Datenschutz wurde auf Formulierungsvorschläge des Blogs zur sozialwissenschaftlichen Methodenberatung zurück gegriffen.  
Vgl. [https://sozmethode.hypotheses.org/292](https://sozmethode.hypotheses.org/292 "https://sozmethode.hypotheses.org/292") (zuletzt geprüft am: 02.05.2020)
