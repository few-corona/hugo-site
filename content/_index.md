---
draft: false
menu:
  main:
    name: "Willkommen"
    url: "/"
    weight: 1
---
Herzlich willkommen,  
schön, dass Sie zu uns gefunden haben!

Wir sind eine Forschungs- und Entwicklungswerkstatt der Ev. Hochschule für Soziale Arbeit und Diakonie *Rauhes Haus* in Hamburg und beschäftigen uns mit dem Erleben und der Bewältigung des Alltags und der Bedeutung sozialer Ungleichheiten in Zeiten der Corona-Pandemie.

Auf dieser Webseite finden Sie einige Informationen zu unserem Forschungsvorhaben, zum Kontext der Forschung, zu Datenschutzfragen sowie unsere Kontaktadresse. Falls Sie sich vorstellen können mit uns ein Interview zu Ihrem Alltag in der aktuellen Situation zu führen, melden Sie sich gern mit einem kurzen *Ich bin dabei* per Mail bei uns.

Solange dies notwendig ist, wird das Interview selbstverständlich nur telefonisch oder via Online-Dienst geführt. Um ihre persönlichen Daten zu schützen, anonymisieren wir alle Interviews.
Wir freuen uns über Ihr Interesse und wünschen Ihnen alles Gute in diesen schweren Zeiten.

Sie erreichen uns unter der Mailadresse:  

{{< mail_address class="mail-address" address="few.corona.eh@rauheshaus.de">}}
