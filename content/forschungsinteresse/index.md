---
title: Forschungsinteresse
draft: false
menu:
  main:
    name: "Forschungsinteresse"
    url: "/forschungsinteresse"
    weight: 5
---

Das Sars-CoV-2 Virus hat den Alltag von allen verändert und die seit März 2020 getroffenen Maßnahmen stellen teilweise eine erhebliche Belastung dar.  

Während Einige als potentielle Risikopatientinnen oder -patienten kaum noch vor die Tür gehen, gilt es für Andere einen Spagat zwischen Kinderbetreuung und Erwerbsarbeit zu meistern. Während einige sich ein baldiges Ende der getroffenen Maßnahmen wünschen, fordern andere die Fortsetzung des *Lock-Downs*.  Während einige in sehr kleinen Wohnungen wohnen, wissen andere nicht wie sie finanziell über die Runden kommen sollen oder sind sehr allein, in dieser schwierigen Zeit.

Kurz gesagt: Alle stehen vor verschiedenen Herausforderungen und jede_r erlebt und bewältigt die Situation unterschiedlich.

Im Rahmen einer Forschungswerkstatt der Hochschule für Soziale Arbeit und Diakonie *Rauhes Haus* beschäftigen wir uns mit diesen vielen unterschiedlichen Lebenssituationen, ihrem Erleben und ihrer Bewältigung.

Hierfür führen wir Interviews mit Bewohner_innen aus unterschiedlichen Stadtteilen und in ganz verschiedenen Lebenslagen. Wir legen hierbei einen besonderen Schwerpunkt auf den Stadtteil Veddel, weil die Forschungswerkstatt ein Kooperationsprojekt mit dem Stadtteilgesundheitszentrum *Poliklinik* auf der Veddel darstellt.
